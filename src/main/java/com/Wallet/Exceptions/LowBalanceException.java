package com.Wallet.Exceptions;


public class LowBalanceException extends Exception {

	private String message;

	
	
	public LowBalanceException() {
		super();
	}



	public LowBalanceException(String message) {
		super();
		this.message = message;
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}
	
	
	 
	
}
