package com.Wallet.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.Wallet.Model.*;
import com.Wallet.ServiceImpl.UserAccountServiceImp;


@RestController
public class UserController {
	
	
	 @Autowired
	 UserAccountServiceImp userRepo;
	
	
	
	    
		@GetMapping(value="api/v1/users")
		public List<UserWallet> getusers() {
			return userRepo.getUsers();
		}
	 
	     @GetMapping(value = "api/v1/user/{id}")
	    public Optional<UserWallet> getUser(@PathVariable Long id) {
	 	return userRepo.getUser(id);
	 	
	    }
	 
	     @PostMapping(value="api/v1/addUser", consumes= {"application/json"})
	     public UserWallet addUser(@RequestBody UserWallet user) {
	    	 userRepo.addUser(user);
	    	 return user;
	    }
	     @DeleteMapping(value="api/v1/user/{id}")
	    public void deleteUser(@PathVariable Long id) {
	    	userRepo.deleteUser(id);
	    }
	      
	     @PutMapping(value="api/v1/user/{id}")
	     public void updateUser(@RequestBody UserWallet user, @PathVariable Long id) {
	    	 userRepo.updateUser(id,user);
	     }
	    
	    
}
