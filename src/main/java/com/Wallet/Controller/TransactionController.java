package com.Wallet.Controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Wallet.DTO.TransactionDTO;
import com.Wallet.DTO.mapper.TransactionMapper;
import com.Wallet.Exceptions.LowBalanceException;
import com.Wallet.Exceptions.UserNotFoundException;
import com.Wallet.Model.Transaction;
//import com.Wallet.Model.UserWallet;
import com.Wallet.ServiceImpl.TransactionServiceImpl;
import com.Wallet.ServiceImpl.UserAccountServiceImp;

@RestController
//@RequestMapping("v1/addMoney")
public class TransactionController {
	
	   @Autowired
	   private UserAccountServiceImp userRepo;
	   
	   @Autowired
	   private TransactionServiceImpl transactRepo;
	   
	   
	   @PostMapping(path = "/apiv1/createTransaction/{id}", consumes = {"application/json"})
		public Transaction addMoney(@PathVariable("id") Long id, @RequestBody TransactionDTO walletDTO) {
		   
		   Transaction saved = null ;
		   
		   try {
				walletDTO.setUserAccountId(id);
				saved = transactRepo.createTransaction(TransactionMapper.dtoToDO(walletDTO));
			} catch (LowBalanceException ex) {
				Logger.getLogger(UserController.class.getName()).log(Level.SEVERE,null, ex);
				return saved;
			} catch (Exception ex) {
				Logger.getLogger(UserController.class.getName()).log(Level.SEVERE,null, ex);
				return saved;
			}
			return saved;
			
			}
	   
	   @GetMapping("api/v1/balance/{id}")
	   public  BigDecimal getBalance(@PathVariable Long id){
		   
		     return transactRepo.balanceByUserAccountID(id);
		  }
	
	   
	   @GetMapping(value="api/v1/transactions")
		public List<Transaction> getTransactions() {
			return transactRepo.Transactions();
		}
	 
	   
	   @GetMapping(value="api/v1/{id}/transactions")
	   public Optional<Transaction> getTransactionsById(@PathVariable Long id) throws UserNotFoundException{
		      return transactRepo.getTransactionsById(id);
		   
	   }
	   
	   
}
