package com.Wallet.Model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//import com.Wallet.DTO.TransactionDTO;
//import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Transaction {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private BigDecimal amount;
	
	private java.util.Date transactionDate;
	
	private Long  transactionReference;
	
	private String details;
	
	@ManyToOne
	@JsonIgnore
	private UserWallet userAccount;
	
	
    public Transaction() {
		super();
	}
	
	
public Transaction(BigDecimal amount,String details, Long reference, Date transactionDate , UserWallet userAccount) {
		super();
		this.amount = amount;
		this.transactionDate = transactionDate;
		this.transactionReference = reference ;
		this.userAccount = userAccount;
		this.details = details;
		
	}

   public Transaction(TransactionBuilder transactionBuilder) {
  
	   id = transactionBuilder.id;
		userAccount = new UserWallet(transactionBuilder.userAccountId);
		amount = transactionBuilder.amount;
		details = transactionBuilder.details;
		transactionDate = transactionBuilder.transactionDate;
		transactionReference = transactionBuilder.transactionReference;
}


public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public Long getTransactionReference() {
		return transactionReference;
	}


	public void setTransactionReference(Long transactionReference) {
		this.transactionReference = transactionReference;
	}
   
	

	public String getDetails() {
		return details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public java.util.Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	
 
	

   public UserWallet getUserAccount() {
		return userAccount;
	}


	public void setUserAccount(UserWallet userAccount) {
		this.userAccount = userAccount;
	}


@Override
public int hashCode() {
	int hash = 5;
	hash = 37 * hash + Objects.hashCode(this.id);
	return hash;
}

@Override
public boolean equals(Object obj) {
	if (this == obj) {
		return true;
	}
	if (obj == null) {
		return false;
	}
	if (getClass() != obj.getClass()) {
		return false;
	}
	final Transaction other = (Transaction) obj;
	if (!Objects.equals(this.id, other.id)) {
		return false;
	}
	return true;
}
	
	
	public static class TransactionBuilder {

		private Long id;
		private Long userAccountId;
		private BigDecimal amount;
		private String details;
		private java.util.Date transactionDate;
		private Long transactionReference;

		public TransactionBuilder setId(Long id) {
			this.id = id;
			return this;
		}

		public TransactionBuilder setUserAccount(Long userAccountId) {
			this.userAccountId = userAccountId;
			return this;
		}

		public TransactionBuilder setAmount(BigDecimal amount) {
			this.amount = amount;
			return this;
		}

		public TransactionBuilder setDetails(String details) {
			this.details = details;
			return this;
		}

		public TransactionBuilder setTransactionDate(java.util.Date date) {
			this.transactionDate = date;
			return this;
		}

		public TransactionBuilder setTransactionReference(Long transactionReference) {
			this.transactionReference = transactionReference;
			return this;
		}

		public Transaction build() {
			return new Transaction(this);
		}

		

	}


	/*
	 * public TransactionDTO getUserAccountId() { // TODO Auto-generated method stub
	 * return getUserAccountId(); }
	 */
	
	
	

}
