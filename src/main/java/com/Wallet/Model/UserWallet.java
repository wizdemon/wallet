package com.Wallet.Model;


import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
public class UserWallet {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String userName;
	
	private String firstName;
	private String lastName;
	private String email;
	
	@Temporal(TemporalType.DATE)
	
    private Date createdDate;
	
	@OneToMany(mappedBy = "userAccount", fetch = FetchType.EAGER)
	@JsonIgnore
    private Set <Transaction> transactions = new HashSet<>();
	
	
	
	
    
    public UserWallet(Long id) {
		super();
		this.id = id;
	}
    
    
    public UserWallet() {
		super();
	}


 public UserWallet(String userName,String firstname, String lastname, String email, Date createdDate ) {
		super();
		this.userName = userName;
		this.createdDate = createdDate;
		this.firstName = firstname;
		this.lastName = lastname;
		this.email = email;
		
	}
   
   


public UserWallet(Long id,String firstname, String lastname , String userName, String email, Date createdDate) {
	super();
	this.id = id;
	this.userName = userName;
	this.email = email;
	this.createdDate = createdDate;
	this.firstName = firstname;
	this.lastName = lastname ;
}

public UserWallet(UserWalletBuilder builder) {
	
	   id = builder.id;
	   userName = builder.userName;
	   firstName = builder.firstName;
	   lastName = builder.lastName;
	   email = builder.email;
	   createdDate = builder.createdDate;
	
}



	public String getEmail() {
	return email;
    }


public void setEmail(String email) {
	this.email = email;
    }


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


    public Long getId() {
		return id;
	}
   
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
    public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Set<Transaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(id,userName,firstName,lastName,email,createdDate);
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserWallet other = (UserWallet) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
		
		
	}
	
	 /**@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder ("UserWallet{");
		sb.append("id=").append(id);
		sb.append(",username='").append(userName).append('\'');
		sb.append(",email='").append(email).append('\'');
		sb.append(",date='").append(createdDate).append('\'');
		sb.append(",firstname='").append(firstName).append('\'');
		sb.append(",lastName='").append(lastName).append('\'');
		sb.append('}');
		return sb.toString();
		
	} */
	public static class UserWalletBuilder{
		
		private Long id;
		private String userName;
		private String firstName;
		private String lastName;
		private String email;
		private Date createdDate;
		
		public Long getId() {
			return id;
		}
		
		public UserWalletBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public UserWalletBuilder setUserName(String userName) {
			this.userName = userName;
			return this;
		}
		public UserWalletBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		public UserWalletBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		public UserWalletBuilder setEmail(String email) {
			this.email = email;
			return this;
		}
		public UserWalletBuilder setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		
		public UserWallet build() {
			
			return new UserWallet(this);
		}
		
		
		
		
		
	}
	
}
