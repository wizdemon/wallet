package com.Wallet.DTO.mapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.Wallet.DTO.UserWalletDTO;
import com.Wallet.Model.UserWallet;

public class UserWalletMapper {
	
	public static UserWallet dtoToDTO(UserWalletDTO usr) {
		
		UserWallet wallet = new UserWallet.UserWalletBuilder().setCreatedDate(new Date()).setId(usr.getId())
				.setUserName(usr.getUserName()).setFirstName(usr.getFirstName()).setLastName(usr.getLastName())
				.setEmail(usr.getEmail()).build();
		  return wallet;
		
	}
	
	public static UserWalletDTO doToDTO(UserWallet usr) {
		
		double balance = usr.getTransactions().stream().mapToDouble(o ->o.getAmount().doubleValue()).sum();
		UserWalletDTO dto = new UserWalletDTO.UserWalletDTOBuilder().setId(usr.getId())
				.setCreatedDate(usr.getCreatedDate()).setUserName(usr.getUserName()).setFirstName(usr.getFirstName())
				.setLastName(usr.getLastName()).setEmail(usr.getEmail()).setBalance(new BigDecimal(balance)).build();
		  return dto;
		}
	
	public static List<UserWalletDTO> doToDTOList(List<UserWallet> wallet){
		return wallet.stream().map(UserWalletMapper::doToDTO).collect(Collectors.toList());
		
	}

}
