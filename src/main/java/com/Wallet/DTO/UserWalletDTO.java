package com.Wallet.DTO;

import java.math.BigDecimal;
import java.util.Date;

import com.Wallet.Model.UserWallet;

public class UserWalletDTO {
	
	
	private Long id;
	private String userName;
	private String firstName;
	private String lastName;
	private String email ;
	private Date createdDate;
	
	private BigDecimal balance;

	public UserWalletDTO() {
		super();
	}
	
	public UserWalletDTO(UserWalletDTOBuilder builder) {
		id = builder.id;
		userName = builder.userName;
		firstName = builder.firstName;
		lastName = builder.lastName;
		email = builder.email;
		createdDate = builder.createdDate;
		balance = builder.balance;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	
	  public static class UserWalletDTOBuilder   {
		
		private Long id;
		private String userName;
		private String firstName;
		private String lastName;
		private String email;
		private Date createdDate;
		private BigDecimal balance;
		
		public UserWalletDTOBuilder setId(Long id) {
			this.id = id;
			return this;
		}
		public UserWalletDTOBuilder setUserName(String userName) {
			this.userName = userName;
			return this;
		}
		public UserWalletDTOBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		public UserWalletDTOBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		public UserWalletDTOBuilder setEmail(String email) {
			this.email = email;
			return this;
		}
		public UserWalletDTOBuilder setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		public UserWalletDTOBuilder setBalance(BigDecimal balance) {
			this.balance = balance;
			return this;
		}
		public UserWalletDTO build() {
			// TODO Auto-generated method stub
			return new UserWalletDTO(this);
		}
		
		
		
	}
	
	
	
	

}
