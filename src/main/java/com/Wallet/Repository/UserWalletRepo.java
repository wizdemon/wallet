package com.Wallet.Repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


import com.Wallet.Model.UserWallet;



@Repository
public interface UserWalletRepo extends JpaRepository<UserWallet, Long>

       {
	
	 Optional<UserWallet> findById(Long id);
	
       }




