package com.Wallet.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.Wallet.Exceptions.LowBalanceException;
import com.Wallet.Exceptions.UserNotFoundException;
import com.Wallet.Model.Transaction;


@Repository
public interface TransactionRepository extends JpaRepository <Transaction, Long>


{
	
	@Query(nativeQuery = true, value = "select * from transaction where transaction_reference = ?")
	Optional<Transaction> getTransactionByRef(Long transactionReference);
    
	/* BigDecimal balanceByUserAccountID(Long accountId); */
	
	@Query(nativeQuery = true, value = "select ifnull(sum(amount),0.00) from transaction where user_account_id = ?")
	BigDecimal getBalance(Long id);
	
	@Query(nativeQuery = true, value = "select * from transaction where user_account_id = ?")
	List<Transaction> getTransactionsForUser(Long id);
	
	
	/*
	 * @Query(nativeQuery = true, value = "select * from transaction")
	 * List<Transaction> getTransactions();
	 */
	
	/*
	 * Transaction createTransaction(Transaction transaction) throws
	 * LowBalanceException;
	 */

	/* BigDecimal getBalance(Long id); */

	
	
}
