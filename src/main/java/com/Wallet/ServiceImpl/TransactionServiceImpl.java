package com.Wallet.ServiceImpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.Wallet.Exceptions.LowBalanceException;
import com.Wallet.Exceptions.UserNotFoundException;
import com.Wallet.Model.Transaction;
import com.Wallet.Model.UserWallet;
import com.Wallet.Repository.TransactionRepository;


@Service
public class TransactionServiceImpl  {

	@Autowired
	private TransactionRepository transRepo;
	
	
	public Object transactionByRef(Long reference) throws UserNotFoundException {
		return transRepo.getTransactionByRef(reference).orElseThrow(
				() -> new UserNotFoundException(String.format("transaction with ref '%d' doesnt exist", reference)));
	}
	
	@Transactional
  public Transaction createTransaction(Transaction transaction) throws LowBalanceException {

		BigDecimal balance = transRepo.getBalance(transaction.getUserAccount().getId());

		if (balance.add(transaction.getAmount()).compareTo(BigDecimal.ZERO) >= 0) {
			return transRepo.save(transaction);
		}

		throw new LowBalanceException(String.format("user's balance is %.2f and cannot perform a transaction of %.2f ",
				balance.doubleValue(), transaction.getAmount().doubleValue()));
		}
  
  public BigDecimal balanceByUserAccountID(Long id) {
		return transRepo.getBalance(id);
	}
  
  public List<Transaction> Transactions() {
		return transRepo.findAll();
		}
  
	/*
	 * public Optional<Transaction>getTransactionsForUser(Long id) { return
	 * transRepo.getTransactionsForUser(id); }
	 */
	
  public Optional<Transaction> getTransactionsById(Long id) {
		
	    return transRepo.findById(id);
	}
	

	
}
