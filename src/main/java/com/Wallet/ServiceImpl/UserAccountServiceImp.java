package com.Wallet.ServiceImpl;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Wallet.Model.UserWallet;
import com.Wallet.Repository.UserWalletRepo;





@Service
public class UserAccountServiceImp {
    
@Autowired	
private UserWalletRepo repo;



public List<UserWallet> getUsers() {
	return repo.findAll();
}

public void addUser(UserWallet user) {
	repo.save(user);
	
}
public Optional<UserWallet> getUser(Long id) {
	
    return repo.findById(id);
}

public void updateUser(Long id, UserWallet user) {
    repo.save(user);
}


public void deleteUser(Long id) {
    repo.deleteById(id);
}

}
